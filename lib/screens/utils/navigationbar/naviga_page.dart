import 'package:app_vapee_transport/screens/book_ticket/book_ticket_fourth_page.dart';
import 'package:app_vapee_transport/screens/book_ticket/book_ticket_page.dart';
import 'package:app_vapee_transport/screens/payment/payment_page.dart';
import 'package:app_vapee_transport/screens/profile/profile_page.dart';
import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';

class NavigationPage extends StatefulWidget {
  @override
  _NavigationPageState createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  int _selectedIndex = 0;
  int counter = 0;
  bool showBadge = true;

  List<GlobalKey<NavigatorState>> _navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  Widget showBadgeCart() {
    return Badge(
      showBadge: true,
      shape: BadgeShape.circle,
      borderRadius: BorderRadius.circular(100),
      badgeColor: Colors.red,
      animationDuration: Duration(milliseconds: 300),
      animationType: BadgeAnimationType.scale,
      position: BadgePosition.topEnd(top: 0, end: 0),
      badgeContent: Text(counter.toString()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab =
            !await _navigatorKeys[_selectedIndex.bitLength]
                .currentState
                .maybePop();

        print(
            'isFirstRouteInCurrentTab: ' + isFirstRouteInCurrentTab.toString());

        // let system handle back button if we're on the first route
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
      //    appBar: AppBar(
      //   backgroundColor: clrBackground,
      //   elevation: 0,
      // ),
        bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.black,
          unselectedItemColor: Colors.black,
          currentIndex: _selectedIndex,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          items: [
            BottomNavigationBarItem(
                icon: new Image.asset(
                  'assets/images/ic_home.png',
                  height: 25,
                  width: 25,
                ),
                title: Text('HOME'),
                activeIcon: new Image.asset(
                  'assets/images/ic_home.png',
                  height: 25,
                  width: 25,
                 color: Colors.blue[900],
                )),
            BottomNavigationBarItem(
                icon: new Image.asset(
                  'assets/images/ic_file.png',
                  height: 25,
                  width: 25,
                ),
                title: Text('File'),
                activeIcon: new Image.asset(
                  'assets/images/ic_file.png',
                  height: 25,
                  width: 25,
                 color: Colors.blue[900],
                )),
            BottomNavigationBarItem(
                icon: Stack(
                  children: <Widget>[
                    showBadgeCart(),
                    new Image.asset(
                      'assets/images/ic_notification.png',
                      height: 25,
                      width: 25,
                    ),
                  ],
                ),
                title: Text('Notification'),
                activeIcon: Stack(
                  children: <Widget>[
                    showBadgeCart(),
                    new Image.asset(
                      'assets/images/ic_notification.png',
                      height: 25,
                      width: 25,
                      color: Colors.blue[900],
                    )
                  ],
                )),
            BottomNavigationBarItem(
                icon: new Image.asset(
                  'assets/images/ic_user.png',
                  height: 25,
                  width: 25,
                ),
                title: Text('Profile'),
                activeIcon: new Image.asset(
                  'assets/images/ic_user.png',
                  height: 25,
                  width: 25,
                  color: Colors.blue[900],
                )),
          ],
          onTap: (index) {
            setState(() {
              _selectedIndex = index;
            });
          },
        ),
        body: Stack(
          children: [
            _buildOffstageNavigator(0),
            _buildOffstageNavigator(1),
            _buildOffstageNavigator(2),
            _buildOffstageNavigator(3),
          ],
        ),
      ),
    );
  }

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, int index) {
    return {
      '/': (context) {
        return [
          BookTicketPage(),
          FourthBookTicketPage(),
          PaymentPage(),
          ProfilePage()
        ].elementAt(index);
      },
    };
  }

  Widget _buildOffstageNavigator(int index) {
    var routeBuilders = _routeBuilders(context, index);
    return Offstage(
      offstage: _selectedIndex != index,
      child: Navigator(
        key: _navigatorKeys[index],
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            maintainState: true,
            builder: (context) => routeBuilders[routeSettings.name](context),
          );
        },
      ),
    );
  }
}
