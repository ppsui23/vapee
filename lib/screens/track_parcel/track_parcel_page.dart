
import 'package:app_vapee_transport/screens/track_parcel/track_parcel_second_page.dart';
import 'package:app_vapee_transport/screens/utils/constraint.dart';
import '../vapee_layout.dart';
import 'package:flutter/material.dart';

class TrackParcelPage extends StatefulWidget {
  @override
  _TrackParcelPageState createState() => _TrackParcelPageState();
}

class _TrackParcelPageState extends State<TrackParcelPage> {
  TextEditingController _trackController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return VapeeLayout(
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
        title: Text(
          'ติดตามพัสดุ',
          style: TextStyle(
            color: Colors.black,
            fontSize: 24,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Image.asset(
                'assets/images/ic_van.png',
                scale: 5,
              ),
              Text(
                'กรอกหมายเลขพัสดุ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 20,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  '1. กรอกรหัสพัสดุที่ได้รับจากใบเสร็จการส่งพัสดุ\n2. กดปุ่ม “ติดตาม”',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(12),
                child: TextField(
                  controller: _trackController,
                  style: TextStyle(fontSize: 20.0),
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    contentPadding: const EdgeInsets.only(
                      left: 10.0,
                      bottom: 2.0,
                      top: 2.0,
                      right: 10,
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    hintText: '1234',
                    hintStyle: TextStyle(fontSize: 22),
                    labelText: 'หมายเลขพัสดุ',
                    labelStyle: TextStyle(
                      fontSize: 20,
                      color: Colors.grey[700],
                    ),
                  ),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ShowcaseDeliveryTimeline()));
                },
                padding: EdgeInsets.symmetric(
                  horizontal: 25,
                  vertical: 6,
                ),
                color: clrBtn,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Text(
                  'ติดตามพัสดุ',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
