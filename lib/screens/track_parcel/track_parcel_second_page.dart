import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:app_vapee_transport/screens/vapee_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_showcase/flutter_showcase.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:timeline_tile/timeline_tile.dart';

class ShowcaseDeliveryTimeline extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return VapeeLayout(
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
        title: Text(
          'ตรวจเช็คพัสดุ',
          style: TextStyle(
            color: Colors.black,
            fontSize: 24,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          _Header(),
          Expanded(child: _TimelineDelivery()),
        ],
      ),
    );
  }
}

class _TimelineDelivery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[
          TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: 0.1,
            isFirst: true,
            indicatorStyle: const IndicatorStyle(
              width: 20,
              color: Color(0xFF27AA69),
              padding: EdgeInsets.all(6),
            ),
            endChild: const _RightChild(
              asset: 'assets/images/ic_delivery.png',
              title: 'รับพัสดุเข้าคลัง',
              message: '30/11/2020  10.47',
            ),
            beforeLineStyle: const LineStyle(
              color: Color(0xFF27AA69),
            ),
          ),
          TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: 0.1,
            indicatorStyle: const IndicatorStyle(
              width: 20,
              color: Color(0xFF27AA69),
              padding: EdgeInsets.all(6),
            ),
            endChild: const _RightChild(
              asset: 'assets/images/ic_delivery-truck.png',
              title: 'อยู่ในระหว่างการจัดส่ง',
              message: '30/11/2020  10.47',
            ),
            beforeLineStyle: const LineStyle(
              color: Color(0xFF27AA69),
            ),
          ),
          TimelineTile(
            alignment: TimelineAlign.manual,
            lineXY: 0.1,
            indicatorStyle: const IndicatorStyle(
              width: 20,
              color: Color(0xFF2B619C),
              padding: EdgeInsets.all(6),
            ),
            endChild: const _RightChild(
              asset: 'assets/images/ic_receive.png',
              title: 'ผู้รับได้รับพัสดุแล้ว',
              message: '30/11/2020  10.47',
            ),
            beforeLineStyle: const LineStyle(
              color: Color(0xFF27AA69),
            ),
            afterLineStyle: const LineStyle(
              color: Color(0xFFDADADA),
            ),
          ),
          // TimelineTile(
          //   alignment: TimelineAlign.manual,
          //   lineXY: 0.1,
          //   isLast: true,
          //   indicatorStyle: const IndicatorStyle(
          //     width: 20,
          //     color: Color(0xFFDADADA),
          //     padding: EdgeInsets.all(6),
          //   ),
          //   endChild: const _RightChild(
          //       disabled: true,
          //       asset: 'assets/delivery/ready_to_pickup.png',
          //       title: 'Ready to Pickup',
          //       message: '30/11/2020  10.47'),
          //   beforeLineStyle: const LineStyle(
          //     color: Color(0xFFDADADA),
          //   ),
          // ),
        ],
      ),
    );
  }
}

class _RightChild extends StatelessWidget {
  const _RightChild({
    Key key,
    this.asset,
    this.title,
    this.message,
    this.disabled = false,
  }) : super(key: key);

  final String asset;
  final String title;
  final String message;
  final bool disabled;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Row(
        children: <Widget>[
          Opacity(
            child: Image.asset(asset, height: 50),
            opacity: disabled ? 0.5 : 1,
          ),
          const SizedBox(width: 16),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontFamily: 'THSarabunNew',
                  color: disabled
                      ? const Color(0xFFBABABA)
                      : const Color(0xFF636564),
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              ),
              const SizedBox(height: 6),
              Text(
                message,
                style: TextStyle(
                  fontFamily: 'THSarabunNew',
                  color: disabled
                      ? const Color(0xFFD5D5D5)
                      : const Color(0xFF636564),
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class _Header extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return
        Padding(
      padding: const EdgeInsets.only(
        top: 30,
        left: 6,
        right: 6,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'เลขใบนำส่งหลัก: 123456',
            style: TextStyle(
              fontFamily: 'THSarabunNew',
              color: Colors.black,
              fontSize: 22,
            ),
          ),
      
        ],
      ),
    );

  }
}



@override
Size get preferredSize => const Size.fromHeight(kToolbarHeight);
