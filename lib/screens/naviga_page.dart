
import 'package:app_vapee_transport/screens/book_ticket/book_ticket_page.dart';
import 'package:app_vapee_transport/screens/home/home_page.dart';
import 'package:app_vapee_transport/screens/payment/payment_page.dart';
import 'package:app_vapee_transport/screens/payment/payment_third.dart';
import 'package:app_vapee_transport/screens/profile/profile_page.dart';
import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';

class NavigationBarPage extends StatefulWidget {
  @override
  _NavigationBarPageState createState() => _NavigationBarPageState();
}

class _NavigationBarPageState extends State<NavigationBarPage> {
  

  int _pageIndex = 0;

  final BookTicketPage _bookTicketPage = BookTicketPage();
  final PaymentPage _paymentPage = PaymentPage();
  final HomePage _homePage = HomePage();
  final PaymentThirdPage _paymentThirdPage = PaymentThirdPage();
  final ProfilePage _profilePage = ProfilePage();

 
  Widget _showPage = BookTicketPage();

  Widget _pageChoose(int page) {
    switch (page) {
      case 0:
        return _bookTicketPage;
        break;
      case 1:
        return _paymentPage;
        break;
      case 2:
        return _homePage;
        break;
      case 3:
        return _paymentThirdPage;
        break;
      case 4:
        return _profilePage;
        break;
      default:
        return Container(
          child: Center(
            child: Text(
              "ไม่พบหน้า",
              style: TextStyle(fontSize: 20),
            ),
          ),
        );
        
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
          index: _pageIndex,
          height: 50,
          items: <Widget>[
            Icon(Icons.menu, size: 30,),
            Icon(Icons.list, size: 30),
            Icon(Icons.home, size: 30),
            Icon(Icons.payment, size: 30),
            Icon(Icons.perm_identity, size: 30),
          ],
          color: Colors.white,
          buttonBackgroundColor: clrBtn,
          backgroundColor: clrBackground,
          animationCurve: Curves.easeInOut,
          animationDuration: Duration(milliseconds: 600),
          onTap: (int tappedIndex) {
            setState(() {
              _showPage = _pageChoose(tappedIndex);
            });
          },
        ),
        body: Container(
          color: Colors.blueAccent,
          child: Center(
            child: _showPage,
          ),
        ));
  }
}
