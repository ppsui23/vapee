import 'dart:io';
import 'package:app_vapee_transport/screens/payment/payment_page.dart';
import 'package:intl/intl.dart';
import 'package:app_vapee_transport/screens/payment/payment_third.dart';
import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:image_picker/image_picker.dart';

class PaymentSecondPage extends StatefulWidget {
  @override
  _PaymentSecondPageState createState() => _PaymentSecondPageState();
}

class _PaymentSecondPageState extends State<PaymentSecondPage> {
  // List<String> _bank = <String>['A', 'B', 'C'];
  // List<String> _target = <String>['a', 'b', 'c'];

  String _valueBank = 'โอนจากธนาคาร';
  String _valueTarget = 'โอนไปยัง';

  DateTime selectedDate = DateTime.now();
  var customFormat = DateFormat('dd-MM-yyyy');
  File file;
  final picker = ImagePicker();

  TimeOfDay selectedTime = TimeOfDay.now();

  Future<Null> chooseImage(ImageSource source) async {
    try {
      var image = await ImagePicker()
          .getImage(source: source, maxWidth: 800.0, maxHeight: 800.0);
      setState(() {
        file = File(image.path);
      });
    } catch (e) {}
  }

  // Future getImage() async {
  //   final pickedFile = await picker.getImage(
  //     source: ImageSource.camera,
  //     maxHeight: 1000,
  //     maxWidth: 1000,
  //     imageQuality: 80,
  //   );

  //   setState(() {
  //     _image = File(pickedFile.path);
  //   });
  // }

  _selectDate(BuildContext context) async {
    final DateTime picked = await showRoundedDatePicker(
      context: context,
      initialDate: selectedDate,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }

  _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedTime)
      setState(
        () {
          selectedTime = picked;
        },
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBackground,
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Text(
                'ยืนยันการชำระเงิน',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
              padding: EdgeInsets.all(12),
              alignment: Alignment.topLeft,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'อัปโหลดหลักฐานการการชำระเงิน',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  InkWell(
                    // onTap: image,
                    child: Center(
                      child: Container(
                        // height: 120,
                        width: 250,
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 3),
                        decoration: BoxDecoration(
                          border: Border.all(),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Row(
                          children: [
                            IconButton(
                                icon: Icon(Icons.add_a_photo),
                                padding: const EdgeInsets.only(left: 5.0),
                                onPressed: () =>
                                    chooseImage(ImageSource.camera)),
                            Container(
                              width: 100.0,
                              height: 100.0,
                              child: file == null
                                  ? Image.asset('images/upload.png')
                                  : Image.file(file),
                            ),
                            IconButton(
                              icon: Icon(Icons.add_photo_alternate),
                              padding: const EdgeInsets.only(right: 5.0),
                              onPressed: () => chooseImage(ImageSource.gallery),
                            ),

                            // _image != null
                            //     ? Image.file(
                            //         _image,
                            //         scale: 2.5,
                            //         fit: BoxFit.fill,
                            //       )
                            //     : Image.asset(
                            //         'assets/images/ic_image.png',
                            //         scale: 2.5,
                            //      ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            InkWell(
              onTap: () => _selectDate(context),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                padding: EdgeInsets.all(12),
                alignment: Alignment.topLeft,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text(
                  "วันที่โอนเงิน:${customFormat.format(selectedDate)}"
                      .split(' ')[0],
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () => _selectTime(context),
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                padding: EdgeInsets.all(12),
                alignment: Alignment.topLeft,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                ),
                child: Text(
                  "เวลาโอนเงิน:${selectedTime.format(context)}".split(' ')[0],
                  style: TextStyle(
                    fontSize: 20,
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  'โอนจากธนาคาร',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                dropdownColor: Colors.white,
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                value: _valueBank,
                items: [],
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  'โอนไปยัง',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                dropdownColor: Colors.white,
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                value: _valueTarget,
                items: [],
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 8, horizontal: 20),
              child: TextFormField(
                keyboardType: TextInputType.phone,
                style: TextStyle(fontSize: 20.0),
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: const EdgeInsets.only(
                    left: 10.0,
                    bottom: 10.0,
                    top: 10.0,
                    right: 10,
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  focusedErrorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.white),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  errorStyle: TextStyle(fontSize: 20),
                  hintText: 'จำนวนเงิน',
                  hintStyle: TextStyle(fontSize: 20),
                  labelText: 'จำนวนเงิน',
                  labelStyle: TextStyle(
                    color: Colors.black87,
                    fontSize: 20,
                  ),
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'กรุณาระบุจำนวนเงิน';
                  }
                  return null;
                },
              ),
            ),
            SizedBox(
              height: 10,
            ),
           Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentThirdPage()),
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                    color: clrBtn,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      'ตกลง',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => PaymentPage()),
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                    color: clrBtn,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      'ยกเลิก',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
