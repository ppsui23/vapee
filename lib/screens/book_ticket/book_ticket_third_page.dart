import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:app_vapee_transport/screens/book_ticket/book_ticket_fourth_page.dart';
import 'package:flutter/material.dart';
import 'book_ticket_second_page.dart';
import 'ticket_seat/ticket_seat.dart';

class ThirdBookTicketPage extends StatefulWidget {
  bool isReserved;
  bool isSelected;

  static var routeName;
  ThirdBookTicketPage({this.isSelected = false, this.isReserved = false});

  @override
  _ThirdBookTicketPageState createState() => _ThirdBookTicketPageState();
}

class _ThirdBookTicketPageState extends State<ThirdBookTicketPage> {
  final String source = 'สุรินทร์';
  final String destination = 'บุรีรัมย์';
  final String dateGo = '';

  final List<String> items1 = <String>['A', 'C', 'D'];
  final List<String> items2 = <String>['A', 'B', 'D'];
  final List<String> items3 = <String>['A', 'C', 'D'];
  final List<String> items4 = <String>['A', 'B', 'D'];
  final List<String> items5 = <String>['A', 'C', 'D'];
  final List<String> items6 = <String>['A', 'B', 'D'];
  final List<String> items7 = <String>['A', 'C', 'D'];
  final List<String> items8 = <String>['A', 'B'];
  String _valueSeat1, _valueSeat2, _valueSeat3, _valueSeat4;
  String _valueSeat5, _valueSeat6, _valueSeat7, _valueSeat8;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBackground,
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Text(
                'เลือกที่นั่ง',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(12),
              padding: EdgeInsets.all(12),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(12)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'เส้นทาง สุรินทร์ - บุรีรัมย์\nเดินทางจาก สุรินทร์ – บุรีรัมย์\nออกเดินทาง วันอาทิตย์ 23 ส.ค. 2563 04.15 น.\nถึง วันอาทิตย์ 23 ส.ค. 2563 05.45 น.',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Container(
                margin: EdgeInsets.symmetric(horizontal: 12),
                padding: EdgeInsets.all(6),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12)),
                child: InkWell(
                    // onTap: () {
                    //   Navigator.push(
                    //     context,
                    //     MaterialPageRoute(
                    //         builder: (context) => FourthBookTicketPage()),
                    //   );
                    // },
                    child: Image.asset('assets/images/ic_driver.png', scale: 12)),
              ),
            ), // Icon Human
            Padding(
              padding: EdgeInsets.all(5.0),
              child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 12),
                      padding: EdgeInsets.all(6),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(12)),
               child: Column(
                 children: [
                     Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                      Text(
                        '1A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '1B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '1c',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '1D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                    ],
                  ),
                  // Second Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '2A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '2B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '2C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: true,
                      ),
                      Text(
                        '2D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                    ],
                  ),
                  // Third  Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '3A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: true,
                        isSelected: true,
                      ),
                      Text(
                        '3B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: false,
                        isSelected: false,
                      ),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '3C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: true,
                      ),
                      Text(
                        '3D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: true,
                      ),
                    ],
                  ),
                  // 4TH Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '4A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '4B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '4C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(
                        isReserved: true,
                      ),
                      Text(
                        '4D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                    ],
                  ),
                  // 5TH Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '5A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '5B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '5C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '5D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                    ],
                  ),
                  // 6TH Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        '6A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '6B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '6C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '6D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                    ],
                  ),
                  // final Row
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                      Text(
                        '7A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '7B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '7C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '7D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                      Text(
                        '8A',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '8B',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20) * 2,
                      ),
                      Text(
                        '8C',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      Text(
                        '8D',
                        style: TextStyle(fontSize: 20),
                      ),
                      TicketSeat(),
                      SizedBox(
                        width: (MediaQuery.of(context).size.width / 20),
                      ),
                    ],
                  ),
                 ],
               ),
              ),
            ),
            // child: Image.asset('assets/images/seats_map.png'),
            //   child: Column(
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: [
            //       Row(
            //         children: [
            //           Container(
            //             margin: EdgeInsets.all(8),
            //             height: 35,
            //             width: 35,
            //             decoration: BoxDecoration(
            //               border: Border.all(width: 0.5),
            //               color: Colors.white,
            //             ),
            //           ),
            //           Text(
            //             '1A',
            //             style: TextStyle(fontSize: 22),
            //           ),
            //         ],
            //       ), // Seat
            //       Row(
            //         children: [
            //           Container(
            //             margin: EdgeInsets.all(8),
            //             height: 35,
            //             width: 35,
            //             decoration: BoxDecoration(
            //               border: Border.all(width: 0.5),
            //               color: Colors.white,
            //             ),
            //           ),
            //           Text(
            //             '2A',
            //             style: TextStyle(fontSize: 22),
            //           ),
            //         ],
            //       ), // Seat
            //       Row(
            //         children: [
            //           Container(
            //             margin: EdgeInsets.all(8),
            //             height: 35,
            //             width: 35,
            //             decoration: BoxDecoration(
            //               border: Border.all(width: 0.5),
            //               color: Colors.red,
            //             ),
            //           ),
            //           Text(
            //             '3A',
            //             style: TextStyle(fontSize: 22),
            //           ),
            //         ],
            //       ), // Seat
            //     ],
            //   ),
            // ),
            // Row(
            //   children: [
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            //       padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
            //       height: 50,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[100],
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //       child: DropdownButton<String>(
            //         value: _valueSeat1,
            //         onChanged: (String value) {
            //           setState(() => _valueSeat1 = value);
            //         },
            //         selectedItemBuilder: (BuildContext context) {
            //           return items1.map<Widget>((String item) {
            //             return Text(item);
            //           }).toList();
            //         },
            //         items: items1.map((String item) {
            //           return DropdownMenuItem<String>(
            //             child: Text(
            //               'ที่นั่ง 1$item',
            //               style: TextStyle(fontSize: 20),
            //             ),
            //             value: item,
            //           );
            //         }).toList(),
            //         hint: Text(
            //           'แถวที่ 1',
            //           style: TextStyle(
            //             fontSize: 20,
            //             color: Colors.black,
            //           ),
            //         ),
            //         dropdownColor: Colors.white,
            //       ),
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            //       padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
            //       height: 50,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[100],
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //       child: DropdownButton<String>(
            //         value: _valueSeat2,
            //         onChanged: (String value) {
            //           setState(() => _valueSeat2 = value);
            //         },
            //         selectedItemBuilder: (BuildContext context) {
            //           return items1.map<Widget>((String item) {
            //             return Text(item);
            //           }).toList();
            //         },
            //         items: items2.map((String item) {
            //           return DropdownMenuItem<String>(
            //             child: Text(
            //               'ที่นั่ง: 2$item',
            //               style: TextStyle(fontSize: 20),
            //             ),
            //             value: item,
            //           );
            //         }).toList(),
            //         hint: Text(
            //           'แถวที่ 2',
            //           style: TextStyle(
            //             fontSize: 20,
            //             color: Colors.black,
            //           ),
            //         ),
            //         dropdownColor: Colors.white,
            //       ),
            //     ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            //       padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
            //       height: 50,
            //       decoration: BoxDecoration(
            //         color: Colors.grey[100],
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //       child: DropdownButton<String>(
            //         value: _valueSeat3,
            //         onChanged: (String value) {
            //           setState(() => _valueSeat3 = value);
            //         },
            //         selectedItemBuilder: (BuildContext context) {
            //           return items2.map<Widget>((String item) {
            //             return Text(item);
            //           }).toList();
            //         },
            //         items: items3.map((String item) {
            //           return DropdownMenuItem<String>(
            //             child: Text(
            //               'ที่นั่ง 3$item',
            //               style: TextStyle(fontSize: 20),
            //             ),
            //             value: item,
            //           );
            //         }).toList(),
            //         hint: Text(
            //           'แถวที่ 3',
            //           style: TextStyle(
            //             fontSize: 20,
            //             color: Colors.black,
            //           ),
            //         ),
            //         dropdownColor: Colors.white,
            //       ),
            //    ),
            //     Container(
            //       margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            //       padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
            //       height: 50,
            //       decoration: BoxDecoration(
            //         color: Colors.white,
            //         borderRadius: BorderRadius.circular(10),
            //       ),
            //       child: DropdownButton<String>(
            //         value: _valueSeat4,
            //         onChanged: (String value) {
            //           setState(() => _valueSeat4 = value);
            //         },
            //         selectedItemBuilder: (BuildContext context) {
            //           return items3.map<Widget>((String item) {
            //             return Text(item);
            //           }).toList();
            //         },
            //         items: items4.map((String item) {
            //           return DropdownMenuItem<String>(
            //             child: Text(
            //               'ที่นั่ง 4$item',
            //               style: TextStyle(fontSize: 20),
            //             ),
            //             value: item,
            //           );
            //         }).toList(),
            //         hint: Text(
            //           'แถวที่ 4',
            //           style: TextStyle(
            //             fontSize: 20,
            //             color: Colors.black,
            //           ),
            //         ),
            //         dropdownColor: Colors.white,
            //       ),
            //     ),
            // ],
            // ),

            Container(
              margin: EdgeInsets.all(8),
              padding: EdgeInsets.all(4),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blueAccent[100]),
              ),
              child: Text(
                'จำนวนที่นั่งที่เลือกทั้งหมด 2 ที่นั่ง',
                style: TextStyle(
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            ),
            Row(
              children: [
                Container(
                  margin: EdgeInsets.all(8),
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    border: Border.all(width: 0.5),
                    color: Colors.white,
                  ),
                ),
                Text(
                  'ว่าง',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    border: Border.all(width: 0.5),
                    color: Colors.red,
                  ),
                ),
                Text(
                  'ขาย',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8),
                  height: 35,
                  width: 35,
                  decoration: BoxDecoration(
                    border: Border.all(width: 0.5),
                    color: Color(0xffF7BB0E),
                  ),
                ),
                Text(
                  'จอง',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => SecondBookTicketPage()),
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                    color: clrBtn,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      'เปลี่ยนเที่ยวรถ',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(padding: EdgeInsets.all(8.0)),
                  RaisedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FourthBookTicketPage()),
                      );
                    },
                    padding: EdgeInsets.symmetric(horizontal: 50, vertical: 5),
                    color: clrBtn,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Text(
                      'ตกลง',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
