import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:app_vapee_transport/screens/book_ticket/book_ticket_third_page.dart';
import 'package:flutter/material.dart';
import 'book_ticket_page.dart';

class SecondBookTicketPage extends StatefulWidget {
  final String source, destination;
  final dateDepature;
  final int numSeat;

  static var routeName;

  SecondBookTicketPage({
    this.source,
    this.destination,
    this.dateDepature,
    this.numSeat,
  });

  @override
  _SecondBookTicketPageState createState() => _SecondBookTicketPageState();
}

class _SecondBookTicketPageState extends State<SecondBookTicketPage> {
  // final names = [
  //   'Aby',
  //   'Aish',
  //   'Ayan',
  //   'Ben',
  //   'Bob',
  //   'Charlie',
  //   'Cook',
  //   'Carline'
  // ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBackground,
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Text(
                'เลือกเที่ยวรถ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(12),
              padding: EdgeInsets.all(12),
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(12)),
              child: Column(
                children: [
                  Row(
                    children: [
                      Image.asset(
                        'assets/images/choose_bus.png',
                        scale: 16,
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'ต้นทาง',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                            ),
                          ),
                          Text(
                            '${widget.source}',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ), // Source
                  Row(
                    children: [
                      Image.asset(
                        'assets/images/gps.png',
                        scale: 16,
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'ปลายทาง',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                            ),
                          ),
                          Text(
                            '${widget.destination}',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ), // Destination
                  Row(
                    children: [
                      Image.asset(
                        'assets/images/calendar.png',
                        scale: 16,
                      ),
                      SizedBox(width: 20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'วันที่เดินทาง',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                            ),
                          ),
                          Text(
                            '${widget.dateDepature}',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ), // Date travel
                ],
              ),
            ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ThirdBookTicketPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.all(12),
                padding: EdgeInsets.all(12),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,

                  // child: Expanded(
                  //   flex: 1,
                  //   child: SizedBox(
                  //     width: 350,
                  //     height: 290,
                  //     child: ListView.builder(
                  //         padding: EdgeInsets.only(top: 10),
                  //         itemCount: names.length,
                  //         reverse: false,
                  //         scrollDirection: Axis.vertical,
                  //         itemBuilder: (context, index) {
                  //           return Container(
                  //               margin: EdgeInsets.all(10),
                  //               padding: EdgeInsets.all(10),
                  //               height: 160,
                  //               decoration: BoxDecoration(
                  //                   color: Colors.white,
                  //                   borderRadius: BorderRadius.circular(12)),
                  //               child: Column(
                  //                 mainAxisAlignment: MainAxisAlignment.start,
                  //                 crossAxisAlignment: CrossAxisAlignment.start,
                  //                 children: [
                  //                   ListTile(

                  //                     title: Text(names[index]),
                  //                   ),
                  //                   Row(
                  //                     mainAxisAlignment:
                  //                         MainAxisAlignment.spaceEvenly,
                  //                     children: [
                  //                       Container(
                  //                         padding: EdgeInsets.symmetric(
                  //                             horizontal: 30, vertical: 8),
                  //                         decoration: BoxDecoration(
                  //                             border:
                  //                                 Border.all(color: Colors.pink)),
                  //                         child: Text(
                  //                           'เวลารถออก\n04.00 น.',
                  //                           style: TextStyle(
                  //                             color: Colors.black,
                  //                             fontSize: 20,
                  //                           ),
                  //                           textAlign: TextAlign.center,
                  //                         ),
                  //                       ), // Source
                  //                       Container(
                  //                         padding: EdgeInsets.symmetric(
                  //                             horizontal: 30, vertical: 8),
                  //                         decoration: BoxDecoration(
                  //                             border:
                  //                                 Border.all(color: Colors.indigo)),
                  //                         child: Text(
                  //                           'ถึงปลายทาง\n05.45 น.',
                  //                           style: TextStyle(
                  //                             color: Colors.black,
                  //                             fontSize: 20,
                  //                           ),
                  //                           textAlign: TextAlign.center,
                  //                         ),
                  //                       ), // Destination
                  //                     ],
                  //                   ),
                  //                 ],
                  children: [
                    Text(
                      'ป. 2 รถปรับอากาศ 32 ที่นั่ง',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.pink)),
                          child: Text(
                            'เวลารถออก\n04.00 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Source
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.indigo)),
                          child: Text(
                            'ถึงปลายทาง\n05.45 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Destination
                      ],
                    ),

                    // Destination
                  ],
                ),
              ),
            ), //ป2

            // return Card(

            //   child: ListTile(
            //     title: Text(names[index]),
            //   ),
            // );
            // return Container(
            //   height: 50,
            //   margin: EdgeInsets.all(2),
            //   child: Center(
            //       child: Text(
            //     '${names.[index]}',
            //     style: TextStyle(fontSize: 18),
            //   )),

            // ),
            //               },
            //         ),
            // ),
            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ThirdBookTicketPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only(top: 2, left: 12, right: 12, bottom: 12),
                padding: EdgeInsets.all(12),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'ป. 1 รถปรับอากาศ 32 ที่นั่ง',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.pink)),
                          child: Text(
                            'เวลารถออก\n04.00 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Source
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.indigo)),
                          child: Text(
                            'ถึงปลายทาง\n05.45 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Destination
                      ],
                    ),

                    // Destination
                  ],
                ),
              ),
            ), //ป2

            InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ThirdBookTicketPage()),
                );
              },
              child: Container(
                margin: EdgeInsets.only(top: 2, left: 12, right: 12),
                padding: EdgeInsets.all(12),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'ป. 2 รถปรับอากาศ 32 ที่นั่ง',
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.pink)),
                          child: Text(
                            'เวลารถออก\n04.00 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Source
                        Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 30, vertical: 8),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.indigo)),
                          child: Text(
                            'ถึงปลายทาง\n05.45 น.',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ), // Destination
                      ],
                    ),

                    // Destination
                  ],
                ),
              ),
            ),
            // ), // รถปรับอากาศ เดาว่าต้องทำเป็น list ว่ามีกี่คัน
            SizedBox(height: 20),
            RaisedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => BookTicketPage()),
                );
              },
              padding: EdgeInsets.symmetric(horizontal: 80, vertical: 5),
              color: clrBtn,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Text(
                'ค้นหาใหม่',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
