import 'package:intl/intl.dart';
import 'package:app_vapee_transport/screens/book_ticket/book_ticket_second_page.dart';
import 'package:app_vapee_transport/screens/utils/constraint.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rounded_date_picker/rounded_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';

class BookTicketPage extends StatefulWidget {
  @override
  _BookTicketPageState createState() => _BookTicketPageState();
}

class Company {
  int id;
  String name;
  Company(this.id, this.name);
  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'สุรินทร์'),
      Company(2, 'ขอนแก่น'),
      Company(3, 'บุรีรัมย์'),
      Company(4, 'สตึก'),
    ];
  }
}

class _BookTicketPageState extends State<BookTicketPage> {
  List<String> _source = <String>['A', 'B', 'C'];
  List<String> _destination = <String>['a', 'b', 'c'];
  String _picked = "จองตั๋ว";

  // List<Company> _companies = Company.getCompanies();
  // List<DropdownMenuItem<Company>> _dropdownMenuItems;
  // Company _selectedCompany;

  // List<int> _numSeats = [1, 2];
  String _valueSource = 'เลือกต้นทาง';
  String _valueDestination = 'เลือกปลายทาง';
  int _valueNumSeats = 1;

  DateTime selectedDate = DateTime.now();
  var customFormat = DateFormat('dd-MM-yyyy');

  _selectDate(BuildContext context) async {
    final DateTime picked = await showRoundedDatePicker(
      context: context,
      initialDate: selectedDate,
      initialDatePickerMode: DatePickerMode.day,
      firstDate: DateTime(2000),
      lastDate: DateTime(2025),
    );
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
      });
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: clrBackground,
      appBar: AppBar(
        backgroundColor: clrBackground,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Center(
              child: Text(
                'ค้นหาเที่ยวเวลาเดินรถ',
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 24,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  '$_valueSource',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                dropdownColor: Colors.white,
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                value: _valueSource,
                items: [],
                onChanged: (value) {
                  setState(() {
                    _valueSource = value;
                  });
                },
              ),
            ), // เลือกต้นทาง Dropdown
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: DropdownButton(
                isExpanded: true,
                hint: Text(
                  '$_valueDestination',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
                dropdownColor: Colors.white,
                icon: Icon(
                  Icons.arrow_drop_down,
                  color: Colors.black,
                ),
                value: _valueDestination,
                items: [],
                onChanged: (value) {
                  setState(() {
                    _valueDestination = value;
                  });
                },
              ),
            ), // เลือกปลายทาง Dropdown
            Container(
              margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 3),
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: InkWell(
                onTap: () => _selectDate(context),
                child: Container(
                  height: 45,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(color: Colors.white),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "วันที่เดินทาง:${customFormat.format(selectedDate)}"
                          .split(' ')[0],
                      style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ),
            ), // เลือกวันที่เดินทาง Date
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'จำนวนที่นั่ง',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  height: 45,
                  margin:
                      EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                  padding:
                      EdgeInsets.only(left: 12, right: 12, top: 3, bottom: 3),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(8)),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: DropdownButton(
                      isExpanded: true,
                      dropdownColor: Colors.white,
                      hint: Text(
                        '$_valueNumSeats',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.black,
                        ),
                      ),
                      icon: Icon(
                        Icons.arrow_drop_down,
                        color: Colors.black,
                      ),
                      value: _valueNumSeats,
                      items: [
                        DropdownMenuItem(
                          child: Text(
                            "1",
                            style: TextStyle(fontSize: 22),
                          ),
                          value: 1,
                        ),
                        DropdownMenuItem(
                          child: Text(
                            "2",
                            style: TextStyle(fontSize: 22),
                          ),
                          value: 2,
                        ),
                      ],
                      onChanged: (value) {
                        setState(() {
                          _valueNumSeats = value;
                        });
                      },
                    ),
                  ),
                ),
              ],
            ),
            Text(
              'สามารถจองตั๋วได้สูงสุด 2 ที่นั่ง/การทำรายการ 1 ครั้ง',
              style: TextStyle(
                color: Colors.black,
                fontSize: 20,
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              margin: EdgeInsets.all(8),
              decoration: BoxDecoration(
                border: Border.all(color: Colors.blue[900]),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RadioButtonGroup(
                    orientation: GroupedButtonsOrientation.HORIZONTAL,
                    activeColor: Colors.red,
                    margin: const EdgeInsets.only(left: 80),
                    onSelected: (String selected) => setState(() {
                      _picked = selected;
                    }),
                    labels: <String>[
                      "ซื้อตั๋ว",
                      "จองตั๋ว",
                    ],
                    labelStyle: TextStyle(fontSize: 20),
                    picked: _picked,
                    itemBuilder: (Radio rb, Text txt, int i) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          // Icon(Icons.public),
                          rb,
                          txt,
                        ],
                      );
                    },
                  ),
                  Text(
                    'หมายเหตุ: หากจองตั๋วต้องชำระเงิน 50 เปอร์เซ็นต์ อีก 50 เปอร์เซ็นต์ ต้องมาชำระเงินที่จุดขายตั๋วก่อนรถออก 1 ชั่วโมงขึ้นไป หากไม่มาชำระเงิน ระบบจะตัดการจองอัติโนมัติ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                    ),
                  ),
                ],
              ),
            ),
            RaisedButton(
              elevation: 0,
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SecondBookTicketPage(
                      source: _valueSource,
                      destination: _valueDestination,
                      dateDepature: '${selectedDate.toLocal()}"'.split(' ')[0],
                      numSeat: _valueNumSeats,
                    ),
                  ),
                );
              },
              padding: EdgeInsets.symmetric(horizontal: 60, vertical: 5),
              color: clrBtn,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Text(
                'ตกลง',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
