import 'package:flutter/material.dart';
import 'const.dart';

class TicketSeat extends StatefulWidget {
  bool isReserved;
  bool isSelected;
 
 
  TicketSeat({this.isSelected = false, this.isReserved = false});

  @override
  _TicketSeatState createState() => _TicketSeatState();
}

class _TicketSeatState extends State<TicketSeat> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: () {
        setState(() {
          // ignore: unnecessary_statements
          !widget.isReserved? widget.isSelected = !widget.isSelected : 2;
        });
      },
      child: Container(
          margin: EdgeInsets.symmetric(horizontal: 7.0, vertical: 5.0),
          width: MediaQuery.of(context).size.width / 15,
          height: MediaQuery.of(context).size.width / 15,
          decoration: BoxDecoration(
              color: widget.isSelected
                  ? tPimaryColor
                  : widget.isReserved ? Colors.red : null,
              border: !widget.isSelected && !widget.isReserved
                  ? Border.all(color: Colors.black, width: 1.0)
                  : null,
              borderRadius: BorderRadius.circular(5.0))),
    );
  }
}
