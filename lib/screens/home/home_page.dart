import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 50.0, left: 10, right: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Vapee Transport',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 42,
                    ),
                  ),
                  Text(
                    'Surin - Khonkean',
                    style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/login');
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(5),
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_login.png',
                          scale: 16,
                        ),
                      ),
                      Text(
                        'เข้าสู่ระบบ/สมัครสมาชิก',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ), // เข้าสู่ระบบ/สมัครสมาชิก
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/bookTicket');
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(5),
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_calendar.png',
                          scale: 16,
                        ),
                      ),
                      Text(
                        'ตารางเวลาเดินรถ',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ), // ตารางเวลาเดินรถ
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/carGps');
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(5),
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_position.png',
                          scale: 16,
                        ),
                      ),
                      Text(
                        'พิกัดตำแหน่งรถ',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ), // พิกัดตำแหน่งรถ
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/trackParcel');
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(5),
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_van.png',
                          scale: 16,
                        ),
                      ),
                      Text(
                        'ติดตามพัสดุ',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ), // ติดตามพัสดุ
            InkWell(
              onTap: () {
                Navigator.pushNamed(context, '/contact');
              },
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(5),
                  color: Colors.white,
                  width: MediaQuery.of(context).size.width * 0.9,
                  child: Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          'assets/images/ic_info.png',
                          scale: 16,
                        ),
                      ),
                      Text(
                        'ติดต่อเรา',
                        style: TextStyle(
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ), // ติดต่อเรา contact us
          ],
        ),
      ),
    );
  }
}
