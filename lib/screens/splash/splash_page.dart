
import 'dart:async';
import 'package:app_vapee_transport/screens/home/home_page.dart';
import 'package:app_vapee_transport/screens/login/login_page.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';


class  SplashPageState extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Timer(Duration(milliseconds: 2500), () {
      Navigator.push(
          context,
          MaterialPageRoute(
            builder: (ctx) => HomePage(),
          ));
    });

    return Scaffold( 
      body: Container(
        // decoration: BoxDecoration(
        //     image: DecorationImage(
        //         image: AssetImage('assets/images/ic_bg.png'), fit: BoxFit.cover),
        //     gradient: LinearGradient(
        //         colors: [Colors.blue, Colors.blue[300]],
        //         begin: Alignment.bottomCenter,
        //         end: Alignment.topCenter)),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 180,
              ),
              // Text(
              //   'VapeeTranSport',
              //   style: TextStyle(
              //       fontFamily: 'THSarabunNew', 
              //       color: Colors.black87, 
              //       fontSize: 40, 
              //       fontWeight: FontWeight.bold,
              //       ),
              // ),
              SizedBox(
                height: 20,
              ),
              Container(
                  height: 180, child: Image(
                    width: 120,
                    height: 120,
                    image: AssetImage(
                      'assets/images/ic_bus_2.png'
                      ))),
              SizedBox(
                height: 180,
              ),
              LinearPercentIndicator(
                alignment: MainAxisAlignment.center,
                width: 240.0,
                lineHeight: 4.0,
                animation: true,
                percent: 1.0,
                animationDuration: 2250,
                backgroundColor: Colors.grey,
                progressColor: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
